from problem_2 import *

policy = LowEnergyPolicy()

with open('medium.policy', 'w') as f:
    for i in range(50000):
        f.write(str(policy.action(state(env,i+1)))+'\n')

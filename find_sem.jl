push!(LOAD_PATH, ".")
using OfficialProblems
using POMDPs
using POMDPToolbox
using PyCall
using ProgressMeter

@pyimport problem_2 as python_funcs

N_SIMS = 10000
N_SIMS_MED = 1000

function calc_sem(policy_vec::Vector, problem::MDP, rng::AbstractRNG)
    policy = VectorPolicy(problem, policy_vec)
    sim = RolloutSimulator(max_steps=1000, rng=rng)
    scores = Array{Float64}(N_SIMS)
    sum = 0.0
    @showprogress for i in 1:N_SIMS
        scores[i] = simulate(sim, problem, policy, initial_state(problem, rng))
        sum += scores[i]
    end
    return sum/N_SIMS, std(scores)/sqrt(N_SIMS)
end

function calc_sem(policy_vec::Vector{Int}, problem::PyObject, rng::AbstractRNG)
    scores = Array(Float64, N_SIMS)
    results = python_funcs.eval_sims(problem, policy_vec, 1000, rand(rng, UInt32), N_SIMS_MED)
    return mean(results), std(results)/sqrt(N_SIMS_MED)
end


policy_vec = convert(Vector{Int}, vec(readdlm("large.policy")))

println("Large:")
@time m, s = calc_sem(policy_vec, p3, MersenneTwister(789))
println("$m ± $s")

policy_vec = convert(Vector{Int}, vec(readdlm("medium.policy")))

println("Medium:")
@time m, s = calc_sem(policy_vec, p2, MersenneTwister(789))
@time println("$m \pm $s")

policy_int_vec = convert(Vector{Int}, vec(readdlm("small.policy")))
action_map = ordered_actions(p1)
policy_vec = [action_map[int_a] for int_a in policy_int_vec]

println("Small:")
@time m, s = calc_sem(policy_vec, p1, MersenneTwister(789))
println("$m ± $s")

push!(LOAD_PATH, dirname(@__FILE__()))

using GWBeliefMDPs
using POMDPs
using POMDPModels
using POMDPToolbox
using DataFrames
using OfficialProblems

num_episodes = 10000
episode_length = 100

rng = MersenneTwister(2016)

policy_rng = MersenneTwister(1988)
policy = RandomPolicy(p3, rng=policy_rng)

data = DataFrame(s=Int[],a=Int[],r=Int[],sp=Int[])

for i in 1:num_episodes
    rec = HistoryRecorder(max_steps=episode_length)
    is = initial_state(p3, rng)
    simulate(rec, p3, policy, is)
    for k in 1:length(rec.state_hist)-1
        s = rec.state_hist[k]
        a = rec.action_hist[k]
        sp = rec.state_hist[k+1]
        r = rec.reward_hist[k]
        push!(data, [state_index(p3, s),
                     a, r,
                     state_index(p3, sp)])
    end
    print("\rDone with episode $i")
end
println()

# println(data)
filename = "large.csv"
writetable(filename, data)

push!(LOAD_PATH, dirname(@__FILE__()))
using GWBeliefMDPs
using POMDPs

mdp = GWBeliefMDP()
p = GoToMostLikely(mdp)

writedlm("large.policy", action(p, GWBelief(mdp, i)) for i in 1:1757600)

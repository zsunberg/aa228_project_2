using POMDPModels
using POMDPToolbox
using POMDPs
using DataFrames

problem = GridWorld(rs=[GridWorldState(3,3), GridWorldState(8,8)],
                    rv=[3.,10.], tp=0.6, terminals=Set{GridWorldState}())

num_episodes = 500
episode_length = 100

rng = MersenneTwister(2016)

policy_rng = MersenneTwister(1988)
policy = RandomPolicy(problem, rng=policy_rng)

data = DataFrame(s=Int[],a=Int[],r=Int[],sp=Int[])

ACTION_INDICES = Dict{Symbol, Int}(:left=>1, :right=>2, :up=>3, :down=>4)

for i in 1:num_episodes
    rec = HistoryRecorder(max_steps=episode_length)
    is = initial_state(problem, rng)
    simulate(rec, problem, policy, is)
    for k in 1:length(rec.state_hist)-1
        s = rec.state_hist[k]
        a = rec.action_hist[k]
        sp = rec.state_hist[k+1]
        push!(data, [state_index(problem, s),
                     ACTION_INDICES[a.direction],
                     reward(problem, s, a, sp),
                     state_index(problem, sp)])
    end
end

# println(data)
filename = "small.csv"
writetable(filename, data)
